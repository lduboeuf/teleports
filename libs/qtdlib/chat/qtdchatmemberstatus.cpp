#include "qtdchatmemberstatus.h"

#include <QDebug>

QTdChatMemberStatus::QTdChatMemberStatus(QObject *parent)
    : QTdObject(parent)
{
}

QTdChatMemberStatusAdministrator::QTdChatMemberStatusAdministrator(QObject *parent)
    : QTdChatMemberStatus(parent)
    , m_canBeEdited(false)
    , m_canChangeInfo(false)
    , m_canPostMessages(false)
    , m_canEditMessages(false)
    , m_canDeleteMessages(false)
    , m_canInviteUsers(false)
    , m_canRestrictMembers(false)
    , m_canPinMessages(false)
    , m_canPromoteMembers(false)
{
    setType(CHAT_MEMBER_STATUS_ADMIN);
}

bool QTdChatMemberStatusAdministrator::canBeEdited() const
{
    return m_canBeEdited;
}

bool QTdChatMemberStatusAdministrator::canChangeInfo() const
{
    return m_canChangeInfo;
}

bool QTdChatMemberStatusAdministrator::canPostMessages() const
{
    return m_canPostMessages;
}

bool QTdChatMemberStatusAdministrator::canEditMessages() const
{
    return m_canEditMessages;
}

bool QTdChatMemberStatusAdministrator::canDeleteMessages() const
{
    return m_canDeleteMessages;
}

bool QTdChatMemberStatusAdministrator::canInviteUsers() const
{
    return m_canInviteUsers;
}

bool QTdChatMemberStatusAdministrator::canRestrictMembers() const
{
    return m_canRestrictMembers;
}

bool QTdChatMemberStatusAdministrator::canPinMessages() const
{
    return m_canPinMessages;
}

bool QTdChatMemberStatusAdministrator::canPromoteMembers() const
{
    return m_canPromoteMembers;
}

void QTdChatMemberStatusAdministrator::unmarshalJson(const QJsonObject &json)
{
    QTdChatMemberStatus::unmarshalJson(json);
    m_canBeEdited = json["can_be_edited"].toBool();
    m_canChangeInfo = json["can_change_info"].toBool();
    m_canPostMessages = json["can_post_messages"].toBool();
    m_canEditMessages = json["can_edit_messages"].toBool();
    m_canDeleteMessages = json["can_delete_messages"].toBool();
    m_canInviteUsers = json["can_invite_users"].toBool();
    m_canRestrictMembers = json["can_restrict_members"].toBool();
    m_canPinMessages = json["can_pin_messages"].toBool();
    m_canPromoteMembers = json["can_promote_members"].toBool();
    emit statusChanged();
}

QTdChatMemberStatusBanned::QTdChatMemberStatusBanned(QObject *parent)
    : QTdChatMemberStatus(parent)
    , m_bannedTil(0)
{
    setType(CHAT_MEMBER_STATUS_BANNED);
}

QDateTime QTdChatMemberStatusBanned::qmlBannedUntilDate() const
{
    return QDateTime::fromTime_t(m_bannedTil);
}

qint32 QTdChatMemberStatusBanned::bannedUntilDate() const
{
    return m_bannedTil;
}

void QTdChatMemberStatusBanned::unmarshalJson(const QJsonObject &json)
{
    QTdChatMemberStatus::unmarshalJson(json);
    m_bannedTil = qint32(json["banned_until_date"].toInt());
    emit statusChanged();
}

QTdChatMemberStatusCreator::QTdChatMemberStatusCreator(QObject *parent)
    : QTdChatMemberStatus(parent)
    , m_isMember(false)
{
    setType(CHAT_MEMBER_STATUS_CREATOR);
}

bool QTdChatMemberStatusCreator::isMember() const
{
    return m_isMember;
}

void QTdChatMemberStatusCreator::unmarshalJson(const QJsonObject &json)
{
    QTdChatMemberStatus::unmarshalJson(json);
    m_isMember = json["is_member"].toBool();
    emit statusChanged();
}

QTdChatMemberStatusLeft::QTdChatMemberStatusLeft(QObject *parent)
    : QTdChatMemberStatus(parent)
{
    setType(CHAT_MEMBER_STATUS_LEFT);
}

QTdChatMemberStatusMember::QTdChatMemberStatusMember(QObject *parent)
    : QTdChatMemberStatus(parent)
{
    setType(CHAT_MEMBER_STATUS_MEMBER);
}

QTdChatMemberStatusRestricted::QTdChatMemberStatusRestricted(QObject *parent)
    : QTdChatMemberStatus(parent)
    , m_isMember(false)
    , m_restrictedUntil(0)
{
    setType(CHAT_MEMBER_STATUS_RESTRICTED);
}

void QTdChatMemberStatusRestricted::unmarshalJson(const QJsonObject &json)
{
    QTdChatMemberStatus::unmarshalJson(json);
    m_isMember = json["is_member"].toBool();
    m_restrictedUntil = qint32(json["restricted_until_date"].toInt());
    emit statusChanged();
}

bool QTdChatMemberStatusRestricted::isMember() const
{
    return m_isMember;
}

QDateTime QTdChatMemberStatusRestricted::qmlRestrictedUntil() const
{
    return QDateTime::fromTime_t(m_restrictedUntil);
}

qint32 QTdChatMemberStatusRestricted::restrictedUntil() const
{
    return m_restrictedUntil;
}

QTdChatMemberStatus *QTdChatMemberStatusFactory::create(const QJsonObject &json, QObject *parent)
{
    QTdChatMemberStatus *result = Q_NULLPTR;
    const QString type = json["@type"].toString();
    if (type == "chatMemberStatusAdministrator") {
        result = new QTdChatMemberStatusAdministrator(parent);
    } else if (type == "chatMemberStatusBanned") {
        result = new QTdChatMemberStatusBanned(parent);
    } else if (type == "chatMemberStatusCreator") {
        result = new QTdChatMemberStatusCreator(parent);
    } else if (type == "chatMemberStatusLeft") {
        result = new QTdChatMemberStatusLeft(parent);
    } else if (type == "chatMemberStatusMember") {
        result = new QTdChatMemberStatusMember(parent);
    } else if (type == "chatMemberStatusRestricted") {
        result = new QTdChatMemberStatusRestricted(parent);
    } else {
        qWarning() << "Received unknown chat member status" << type;
    }

    if (result) {
        result->unmarshalJson(json);
    }

    return result;
}
