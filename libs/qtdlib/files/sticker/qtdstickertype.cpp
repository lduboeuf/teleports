#include "qtdstickertype.h"

#include <QDebug>

QTdStickerType::QTdStickerType(QObject *parent)
    : QTdObject(parent)
{

}


QTdStickerTypeStatic::QTdStickerTypeStatic(QObject *parent)
    : QTdStickerType(parent)
{
    setType(STICKER_TYPE_STATIC);
}

QTdStickerTypeAnimated::QTdStickerTypeAnimated(QObject *parent)
    : QTdStickerType(parent)
{
    setType(STICKER_TYPE_ANIMATED);
}

QTdStickerTypeVideo::QTdStickerTypeVideo(QObject *parent)
    : QTdStickerType(parent)
{
    setType(STICKER_TYPE_VIDEO);
}

QTdStickerTypeMask::QTdStickerTypeMask(QObject *parent)
    : QTdStickerType(parent)
{
    setType(STICKER_TYPE_MASK);
}


QTdStickerType *QTdStickerTypeFactory::create(const QJsonObject &json, QObject *parent)
{
    const QString type = json["@type"].toString();

    QTdStickerType *result;

    if (type == "stickerTypeStatic") {
        result = new QTdStickerTypeStatic(parent);
    } else if (type == "stickerTypeAnimated") {
        result = new QTdStickerTypeAnimated(parent);
    } else if (type == "stickerTypeVideo") {
        result = new QTdStickerTypeVideo(parent);
    } else if (type == "stickerTypeMask") {
        result = new QTdStickerTypeMask(parent);
    } else {
        qWarning() << "Received unknown sticker type" << type << json;
        return new QTdStickerType(parent);
    }
    result->unmarshalJson(json);
    return result;
}