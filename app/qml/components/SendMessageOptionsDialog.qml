import QtQuick 2.9
import QuickFlux 1.1
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.3

import Lomiri.Components 1.3 as UITK
import Lomiri.Components.Popups 1.3
import Lomiri.Components.Pickers 1.3

import QTelegram 1.0
import "../actions"
import "../components"
import "../stores"

PopupDialog {
    property string message
    property string replyId
    property int sendDate: 0
    title: i18n.tr("Send Message options")
    confirmButtonText: i18n.tr("Send")

    RowLayout {
        CheckBox {
            id: pushNotification
            checked: true
        }
        UITK.Label {
            text: i18n.tr("Send push notification")
        }       
    }

    UITK.Label {
        text: i18n.tr("Schedule message:")
    }

    ButtonGroup {
        id: scheduleButtonsGroup
    }
    GridLayout {
        columns: 2
        rows: 4
        width: units.gu(32)
        RadioButton {
            id: deferralNone
            ButtonGroup.group: scheduleButtonsGroup
            checked: true
            onCheckedChanged: if (checked) {
                sendDate = 0;
            }
        }
        UITK.Label {
            text: i18n.tr("Send immediately")
        }
        RadioButton {
            id: deferralDateTime
            ButtonGroup.group: scheduleButtonsGroup
        }
        UITK.Label {
            text: i18n.tr("Wait until specified date and time")
        }
        RadioButton {
            id: deferralRecipient
            ButtonGroup.group: scheduleButtonsGroup
            onCheckedChanged: if (checked) { sendDate = -1; }
        }
        UITK.Label {
            text: i18n.tr("Wait until the receiver goes online")
        }
    }
    AdvancedDateTimePicker {
        id: deferralDateTimeValue
        visible: deferralDateTime.checked
    }    
    onConfirmed: {
        if (deferralDateTime.checked) {
            sendDate = Math.round(deferralDateTimeValue.date.getTime() / 1000);
        }
        AppActions.chat.sendMessageWithOptions(message, !pushNotification.checked, sendDate, replyId);
    }

}
